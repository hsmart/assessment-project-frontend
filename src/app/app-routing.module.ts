import { InvitationModule } from './invitation/invitation.module';
import { Routes } from '@angular/router/src/config';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InvitationComponent } from './invitation/invitation/invitation.component';


const appRoutes: Routes = [
  {
    path: '',
    component: InvitationComponent
 },
  {
    path: 'questionnaire',
    loadChildren: './questionnaire/questionnaire.module#QuestionnaireModule'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false, // <-- debugging purposes only
      }
    ), InvitationModule
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }



