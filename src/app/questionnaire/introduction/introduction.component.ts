import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, EmailValidator } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-introduction',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.css']
})
export class IntroductionComponent implements OnInit {

  constructor(private fb: FormBuilder, private sharedService: SharedService, private route: ActivatedRoute, private router: Router
    ) { }
  introForm: FormGroup;
  submitted = false;


  ngOnInit() {
    this.introForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, EmailValidator]]
     });
  }
  onSubmit() {
   this.submitted = true;
   if (!this.introForm.valid) { return; }
   this.submitted = false;
   this.sharedService.name = this.introForm.value.name;
   this.sharedService.email = this.introForm.value.email;
   this.router.navigate(['../questions'], {relativeTo: this.route});
  }

  get f() { return this.introForm.controls; }

}
