import { IntroductionComponent } from './introduction/introduction.component';
import { NgModule } from '@angular/core';
import { QuestionnaireComponent } from './questionnaire/questionnaire.component';
import { RouterModule, Routes } from '@angular/router';
import { SubmitComponent } from './submit/submit.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {
    path: ':inv',
    component: MainComponent,
    children: [
      {
        path: '',
        component: IntroductionComponent
      },
      {
        path: 'questions',
        component: QuestionnaireComponent
      },
      {
        path: 'submit',
        component: SubmitComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionnaireRoutingModule { }
