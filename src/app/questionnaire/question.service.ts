import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Question } from './model/question.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  uri = environment.BASE_URL + 'question';


  constructor(private http: HttpClient) {

  }

  public getAllQuestion(): Observable<Question[]> {
       return this.http.get<Question[]>(this.uri);

  }
}
