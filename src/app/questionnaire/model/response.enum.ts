export enum ResponseEnum {
  YES = 'Y',
  PARTLY = 'P',
  NO = 'N'
}
