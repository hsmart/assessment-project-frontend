import { ResponseEnum } from './response.enum';

export interface Question {
    code: string;
    label: string;
    response?: ResponseEnum;
}
