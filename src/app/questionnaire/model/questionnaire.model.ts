import { Response } from './response.model';

export interface Questionnaire {
  invitation_ref: string ;
  name: string;
  email: string;
  responses: Response[];
}
