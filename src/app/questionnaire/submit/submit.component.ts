import { Questionnaire } from './../model/questionnaire.model';
import { QuestionnaireService } from './../questionnaire.service';
import { SharedService } from './../shared.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-submit',
  templateUrl: './submit.component.html',
  styleUrls: ['./submit.component.css']
})
export class SubmitComponent implements OnInit {

  constructor(private sharedService: SharedService, private questionnaireService: QuestionnaireService) { }

  loading = false;
  success = false;
  submited = false;

  ngOnInit() {
  }
  sendQuestionnaire() {
    this.loading = true;
    this.submited = true;
    const questionnaire: Questionnaire = {
      invitation_ref: this.sharedService.invitation_ref,
      name: this.sharedService.name,
      email: this.sharedService.email,
      responses: this.sharedService.questions.map(q => ({question: q.code, answer: q.response })),
    };
    this.questionnaireService.addQuestionnaire(questionnaire).subscribe(isOk => {
      this.loading = false;
      this.success = isOk;
    });
  }
}
