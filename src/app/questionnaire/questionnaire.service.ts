import { Questionnaire } from './model/questionnaire.model';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { catchError, flatMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuestionnaireService {

  uri = environment.BASE_URL + 'questionnaire/add';

  constructor(private http: HttpClient) { }

   public addQuestionnaire(questionnaire: Questionnaire): Observable<boolean> {
    return this.http.post<void>(this.uri, questionnaire).pipe(
      flatMap(res => of(true)),
     catchError(() => of(false))
   );
   }

}
