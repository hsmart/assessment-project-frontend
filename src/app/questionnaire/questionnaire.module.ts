import { QuestionnaireRoutingModule } from './questionnaire-routing.module';
import { QuestionService } from './question.service';
import { QuestionnaireService } from './questionnaire.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionnaireComponent } from './questionnaire/questionnaire.component';
import { ReactiveFormsModule } from '@angular/forms';
import { IntroductionComponent } from './introduction/introduction.component';
import { SubmitComponent } from './submit/submit.component';
import { MainComponent } from './main/main.component';

@NgModule({
  declarations: [QuestionnaireComponent, IntroductionComponent, SubmitComponent, MainComponent],
  imports: [
    CommonModule, ReactiveFormsModule, QuestionnaireRoutingModule
  ],
  providers: [QuestionnaireService, QuestionService],
  exports: [QuestionnaireComponent]
})
export class QuestionnaireModule { }
