import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private sharedService: SharedService,  private route: ActivatedRoute) { }

  ngOnInit() {
    this.sharedService.invitation_ref = this.route.snapshot.paramMap.get('inv');
  }

}
