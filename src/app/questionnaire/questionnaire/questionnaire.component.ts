import { QuestionnaireService } from './../questionnaire.service';
import { ResponseEnum } from '../model/response.enum';
import { QuestionService } from './../question.service';
import { Component, OnInit } from '@angular/core';
import { Question } from '../model/question.model';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.css']
})
export class QuestionnaireComponent implements OnInit {

  constructor(private questionService: QuestionService,
     private route: ActivatedRoute, private questionnaireService: QuestionnaireService,
     private sharedService: SharedService, private router: Router) { }

  step = 0;
  questions: Question[];
  responseEnum = ResponseEnum;
  mode = '';



  ngOnInit() {
    if (this.sharedService.questions.length) {
      this.questions = this.sharedService.questions;
    } else {  this.questionService.getAllQuestion().subscribe(qs => this.questions = qs); }

  }
  setResponse(response) {
    this.questions[this.step].response = response;
    if ( this.step + 1 < this.questions.length) {
      this.step = this.step + 1;
   } else {
     this.sharedService.questions = this.questions;
     this.router.navigate(['../submit'], {relativeTo: this.route});
   }
  }
  back() {
    if ( this.step - 1 >= 0) {
      this.step = this.step - 1;
    } else {
      this.sharedService.questions = this.questions;
      this.router.navigate(['../'], {relativeTo: this.route});
    }
  }

  }



