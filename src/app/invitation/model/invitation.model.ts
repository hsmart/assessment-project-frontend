export interface Invitation {
    invited_email: string;
    evaluator_email: string;
}
