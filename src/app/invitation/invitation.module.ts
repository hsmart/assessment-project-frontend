import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvitationComponent } from './invitation/invitation.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [InvitationComponent],
  imports: [
    CommonModule, ReactiveFormsModule
  ],
  exports: [InvitationComponent]
})
export class InvitationModule {}
