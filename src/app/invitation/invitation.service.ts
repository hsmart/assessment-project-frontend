import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, flatMap } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { Invitation } from './model/invitation.model';

@Injectable({
  providedIn: 'root'
})
export class InvitationService {

  uri = environment.BASE_URL + 'invitation/add';
  constructor(private http: HttpClient) { }

  public addInvitation(invitation: Invitation): Observable<boolean> {
    return this.http.post<void>(this.uri, invitation).pipe(
      flatMap(res => of(true)),
     catchError(() => of(false))
   );
  }

}
