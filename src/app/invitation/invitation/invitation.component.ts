import { InvitationService } from './../invitation.service';
import { Component, OnInit } from '@angular/core';
import { EmailValidator, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.css']
})
export class InvitationComponent implements OnInit {

  constructor(private invitationService: InvitationService, private fb: FormBuilder) { }

  invitationForm: FormGroup;
  submitted = false;
  loading = false;
  success = false;
  msg = false;

  ngOnInit() {
    this.invitationForm = this.fb.group({
      invited_email: ['', [Validators.required, EmailValidator]],
      evaluator_email: ['', [Validators.required, EmailValidator]]
     });
  }
  get f() { return this.invitationForm.controls; }
 public onSubmit() {
   this.submitted = true;
   if (!this.invitationForm.valid) { return; }
   this.loading = true;
    this.invitationService.addInvitation(this.invitationForm.value).subscribe(isOk =>{
      this.success = isOk;
      this.loading = false;
      this.msg = true;
      this.submitted = false;
    });
  }
}
